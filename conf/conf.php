<?php 

return array(

	//数据库配置
	"mysql_connect_url"				=>	"192.168.2.117",
	"mysql_connect_root"			=>	"root",
	"mysql_connect_password"		=>	"123456",
	"mysql_connect_db_name"			=>	"volunteer_pu",
	
	//smarty 配置	
	"smarty_caching"				=>	false,  //是否开启模版缓存
	"smarty_cache_lifetime"			=>	120,	//缓存文件有效时间，单位为 秒
	"smarty_template_extension"		=>  "html",	//模版文件后缀名

	//memcache 配置
	"memcache_open"					=> false,	//是否开启memcache,默认 false
	"mem_connect"					=> "127.0.0.1", //memcache 服务器地址。
	"mem_port"						=> 11211,		//服务器端口
	"mem_compressed"				=> 0,	//缓存数据压缩比例。 0 压缩， 1，会有问题。 2，压缩
	"mem_life_time"					=> 3600,	//缓存数据有效期单位 毫秒

	);



 ?>