<?php 

include_once("libs/Smarty/Smarty.class.php"); //包含smarty类文件 

class BaseController{

	private $smarty;

	private $extension;

	public function __construct(){

		$this->smarty=new Smarty(); //建立smarty实例对象$smarty 

		$this->smarty->__set('template_dir',"./view"); //设置模板目录 
		$this->smarty->__set('compile_dir',"./Runtime/templates_c");//设置编译目录 
		$this->smarty->__set('cache_dir',"./Runtime/cache");//缓存目录 


		//读取配置文件
		$conf=include "conf/conf.php";
		
		$this->smarty->cache_lifetime = $conf["smarty_cache_lifetime"]; //缓存时间 
		$this->smarty->caching = $conf["smarty_caching"]; //缓存方式 
		$this->extension=$conf["smarty_template_extension"]; //模版文件后缀名

	}

	//设置变量
	public function assign($key,$value){

		$this->smarty->assign($key,$value);
	}


	//显示页面
	public function display($name){

		$name=$name.".".$this->extension;

		$this->smarty->display($name);

	}
}

 ?>