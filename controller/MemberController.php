<?php 

include_once("model/MemberModel.php");
include_once("BaseController.php");

//member控制器
class MemberController extends BaseController {


	public function index(){

		echo "this is  member controller index function";
	}



	public function all(){

		//加载业务对象
		$member=new MemberModel();

		$data=$member->all();

		//输出数据到页面
		$this->assign("members",$data);
		
		$this->display("member/all");
	}



	public function info($id){

		if(empty($id)){

			throw new Exception("member id is null", 1);
			
		}

		$member=new MemberModel();

		$data=array();

		$result_array=$member->info($id);

		if(!empty($result_array)) {

			$data=$result_array[0];
		}

		$this->assign("member",$data);

		$this->display("member/info");

	}


}

 ?>