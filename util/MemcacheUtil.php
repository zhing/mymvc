<?php 


//数据缓存工具类
class MemcacheUtil{

	private $mem_connect;
	private $mem_port;
	private $mem_compressed;
	private $mem_life_time;


	public function __construct(){

		$conf= include "conf/conf.php";

		$this->mem_connect=$conf["mem_connect"];
		$this->mem_port=$conf["mem_port"];
		$this->mem_compressed=$conf["mem_compressed"];
		$this->mem_life_time=$conf["mem_life_time"];
	}


	public function save($key,$data){

		$mem=new Memcache;

		$mem->connect($this->mem_connect,$this->mem_port);

		$mem->set($key,$data,$this->mem_compressed,$this->mem_life_time);

		$mem->close();

	}


	public function get($key){

		$mem=new Memcache;

		$mem->connect($this->mem_connect,$this->mem_port);

		$data=$mem->get($key);

		$mem->close();

		return $data;
	}


}



 ?>