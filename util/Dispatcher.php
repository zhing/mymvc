<?php 


//路由控制器
class Dispatcher{

	//执行请求url分析，转发用户请求
	public function dispatch(){

		//接收用户请求，分解action,function ,parameter 
		//假设用户请求url为 ：http://127.0.0.1/php-test/index.php/member/info/1.html
		$request_url=$_SERVER['REQUEST_URI'];

		//分离url，获得 action, function ,parameter
		$request_array=explode("/", $request_url);

		// print_r($request_array);

		//如果没有设置actiong值，默认Index action 	
		if(isset($request_array[3])){

			$action_name=$request_array[3];

			if(empty($action_name)) $action_name="Index";

		}else{

			$action_name="Index";
		}


		
		//如果没有设置function 值， 默认index function
		if(isset($request_array[4])){

			$function_name=$request_array[4];

			if(empty($function_name)) $function_name="index";

		}else{

			$function_name="index";
		}

		$parameter="";

		//获取参数
		if(isset($request_array[5])){

			$parameter=$request_array[5];
		}

		// echo "action_name:".$action_name."<br>";

		// echo "function_name:".$function_name."<br>";

		// echo "parameter:".$parameter."<br>";

		//按用户请求的action ，加载对应 Controller，如果文件没找到，提示用户请求路径不存在
		$action_fileName=$action_name."Controller.php";

		$action_className=$action_name."Controller";

		$action_filePath="controller/".$action_fileName;

		try {

			//检查文件是否存在，如果对应文件不存在，提示用户页面不存在
			if(!file_exists($action_filePath)){

				throw new Exception($request_url."  page not found", 404);
			}

			//加载对应action 文件
			include $action_filePath;

			//实例化 controller 类
			$controller =new $action_className();	

			//检查请求的 function 是否存在
			if(!method_exists($controller, $function_name)){

				throw new Exception($request_url."  page not found", 404);
			}

			//执行action中的对应方法
			$controller->$function_name($parameter);
			
		} catch (Exception $e) {

			//显示错误信息
			echo "<h3>Error Page<h3>";
			echo "<h4>Code:".$e->getCode()." <h4>";
			echo "<h4>Message:".$e->getMessage()." <h4>";
			echo "<h4>in:".$e->getFile()."  [line:".$e->getLine()."] <h4>";
			
		}

	}


}





 ?>